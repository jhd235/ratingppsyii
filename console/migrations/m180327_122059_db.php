<?php

use yii\db\Migration;

/**
 * Class m180327_122059_db
 */
class m180327_122059_db extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180327_122059_db cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180327_122059_db cannot be reverted.\n";

        return false;
    }
    */
}
