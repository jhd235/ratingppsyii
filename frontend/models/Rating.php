<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rating".
 *
 * @property int $id
 * @property string $pokazatel
 * @property int $maxBall
 * @property string $uslovie
 */
class Rating extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rating';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pokazatel', 'maxBall', 'uslovie'], 'required'],
            [['maxBall'], 'integer'],
            [['pokazatel', 'uslovie'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pokazatel' => 'Pokazatel',
            'maxBall' => 'Max Ball',
            'uslovie' => 'Uslovie',
        ];
    }
}
