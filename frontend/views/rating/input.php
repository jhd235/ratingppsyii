<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Rating;
use app\models\Teacher;
use yii\jui\AutoComplete;
use yii\web\JsExpression;



/* @var $this yii\web\View */
/* @var $model app\models\Rating */
/* @var $form ActiveForm */

    $data = Teacher::find()
    ->select(['lname as value', 'lname as  label','id as id'])
    ->asArray()
    ->all();
echo $data[1]['value']."<br>";
foreach ($data as $i) {
echo "print_r(i):  ";
print_r($i['id']);
//echo $i->value;
echo "<br>";
}
//    print_r($data[1]->value);//exit;


?>

<div class="rating-input">
ФИО
    <?php 

    echo AutoComplete::widget([
    'name' => 'Teacher',
    'id' =>'ddd',
    'clientOptions' => [
    'source' => $data,
    'autoFill'=>true,
    'minLength'=>'1',
'select' => new JsExpression("function( event, ui ) {
			        $('#result-teacher_id').val(ui.item.id);//#memberssearch-family_name_id is the id of hiddenInput.
			     }")
     ],
     ]);    

	$modelRating = new app\models\Rating; //explicitly define model
	$form = ActiveForm::begin(); ?>
	<?= Html::activeDropDownList($modelRating, 'id',ArrayHelper::map(Rating::find()->all(), 'id', 'pokazatel')) ?>
        
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>


<?php

$cars = array("Volvo", "BMW", "Toyota");

?>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
  <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<form id="frm" method="post">
<input id="cartag" type="text" name="car">
</form>
<script>

$(function() {
    var availableTags =  <?php echo json_encode($data); ?>;
    $( "#cartag" ).autocomplete({
    source: availableTags
    });
});

</script>



</div><!-- rating-input -->

<script type="text/javascript">

// Function to change the content of t2
function modifyText() {
	
	//alert(el.value);
document.getElementById("result-rating_id").value = el.value;
  }

function onFIOchange() {
	
	//alert(ddd.id);
//document.getElementById("result-teacher_id").value = document.getElementById("ddd").value;
  }

// add event listener to table
var el = document.getElementById('rating-id');
el.addEventListener("change", modifyText, false);

var ddd = document.getElementById('ddd');
//ddd.addEventListener("change", onFIOchange, false);

</script>

<div class="rating-form">

    <?php 
$model = new app\models\Result;

$form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'teacher_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rating_id')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
